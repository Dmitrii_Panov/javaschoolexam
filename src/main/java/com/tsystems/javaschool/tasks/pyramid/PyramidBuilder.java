package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int[][] pyramid;
        int row = checkPyramidAndGetRow(inputNumbers.size());
        if (row < 0 || inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }
        Collections.sort(inputNumbers);
        int width = (row * 2) - 1;

        //index of List
        int index = 0;
        pyramid = new int[row][width];
        for (int i = 0; i < row; i++) {
            // count of non zero elements in i row
            // write elements
            //     *
            //    * *
            //   * * *
            int count = 1 + i;
            int left = ((width / 2) - i);
            while (count > 0) {
                pyramid[i][left] = inputNumbers.get(index++);
                left += 2;
                count--;
                }
            }
        return pyramid;
    }

    private int checkPyramidAndGetRow(int length) {
        // n(n+1) = 2*length
        int n;
        n = (int) (Math.sqrt(1 + 4 * 2 * length)) / 2;
        if ((n * (n + 1)) == 2 * length) {
            return n;
        }
        return -1;
    }
}