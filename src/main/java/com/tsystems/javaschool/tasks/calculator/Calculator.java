package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.LinkedList;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        String answer;
        if (statement == null || statement.isEmpty()){
            return null;
        }
        try{
            String rpo = getRPO(statement);
            answer = getAnswer(rpo);
        }
        catch (Exception e){
            return null;
        }
        return answer;
    }

    private String getAnswer(String rpo) {
        double result = 0;
        LinkedList<Double> listDigits = new LinkedList<>();
        for (int i = 0; i <rpo.length() ; i++) {
            if (Character.isDigit(rpo.charAt(i))){
                String digit = "";
                while (!isSeparator(rpo.charAt(i))){
                    digit+=rpo.charAt(i);
                    i++;
                    if (i == rpo.length()){
                        break;
                    }
                }
                listDigits.add(Double.parseDouble(digit));
                i--;
            }
            else if (isOperator(rpo.charAt(i))) {
                double b = listDigits.removeLast();
                double a = listDigits.removeLast();
                char c = rpo.charAt(i);
                if (b == 0 && c == '/'){
                    throw new ArithmeticException();
                }
                switch (c){
                    case ('+'): result=(a + b);
                        break;
                    case ('-'): result=a - b;
                        break;
                    case ('*'): result=a * b;
                        break;
                    case ('/'): result=a / b;
                        break;
                }
                listDigits.add(result);
            }
        }
        NumberFormat  nf = new DecimalFormat("#.######");
        // change 2,56 => 2.56
        String out = nf.format(result);
        out = out.replaceAll(",",".");
        return out;
    }

    private String getRPO(String statement) {
        String outputRPO = "";
        LinkedList<Character> listOper = new LinkedList<>();
        for (int i = 0; i < statement.length(); i++) {
            char c = statement.charAt(i);

            if (isSeparator(c)) {
                continue;
            }

            if (isOperator(c)) {
                if ('(' == c) {
                    listOper.add(c);
                } else if (')' == c) {
                    while (listOper.getLast() != '(') {
                        outputRPO += listOper.removeLast();
                    }
                    listOper.removeLast();
                } else {
                    if (!listOper.isEmpty()) {
                        if (getPriority(c) <= getPriority(listOper.getLast())) {
                            outputRPO+= listOper.removeLast();
                        }
                    }
                    listOper.add(c);
                }
            }

            if (Character.isDigit(c)){
                String digit = "";
                while (!isSeparator(statement.charAt(i)) && !isOperator(statement.charAt(i))){
                    if (('.' == c) && !digit.contains(Character.toString(statement.charAt(i)))){
                        digit+=statement.charAt(i);
                        i++;
                    }
                    digit+=statement.charAt(i);
                    i++;
                    if (i == statement.length()){
                        break;
                    }
                }
                outputRPO+=digit + " ";
                i--;
            }
        }
        while (!listOper.isEmpty()){
            outputRPO+=listOper.removeLast();
        }
        return outputRPO;
    }

    private  int getPriority(char c) {
        switch (c){
            case ('+'):
            case ('-'):
                return 1;
            case ('/'):
            case ('*'):
                return 2;

        }
        return 0;
    }

    private boolean isOperator(char c) {
        return "+-/*()".contains(Character.toString(c));
    }

    private boolean isSeparator(char c) {
        return ' ' == c;
    }
}
